﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      16/02/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema2
{
    class Barco
    {
        #region Atributos
        public enum tipos { Ligeras,Mercantes,MercantesGiga,Crucero }
        string tipo;
        int posicion;
        int prioridad;
        int eslora;
        string imagen;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor básico de la clase Barco
        /// </summary>
        public Barco()
        {

        }

        /// <summary>
        /// Constructor base de la clase Barco
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="posicion"></param>
        /// <param name="prioridad"></param>
        /// <param name="eslora"></param>
        /// <param name="imagen"></param>
        public Barco(string tipo, int posicion, int prioridad, int eslora, string imagen)
        {
            this.tipo = tipo;
            this.posicion = posicion;
            this.prioridad = prioridad;
            this.eslora = eslora;
            this.imagen = imagen;
        }
        #endregion

        #region Metodos

        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve el tipo de barco
        /// </summary>
        public string Tipo
        {
            get
            {
                return tipo;
            }
        }

        /// <summary>
        /// Propiedad que devuelve el nombre del panel donde esta el picturebox del barco
        /// </summary>
        public string Imagen
        {
            get
            {
                return imagen;
            }
        }
        
        /// <summary>
        /// Propiedad que devuelve la posición del barco (Mirar recorrido)
        /// </summary>
        public int Posicion
        {
            get
            {
                return posicion;
            }
            set
            {
                posicion = value;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la prioridad numerica del barco
        /// </summary>
        public int Prioridad
        {
            get
            {
                return prioridad;
            }
        }

        /// <summary>
        /// Propiedad que devuelve los metros de eslora del barco
        /// </summary>
        public int Eslora
        {
            get
            {
                return eslora;
            }
        }
        #endregion
    }
}
