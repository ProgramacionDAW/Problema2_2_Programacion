﻿namespace Problema2
{
    partial class CreaBarco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ltitulo = new System.Windows.Forms.Label();
            this.lpriori = new System.Windows.Forms.Label();
            this.bPrioridad = new System.Windows.Forms.NumericUpDown();
            this.ltam = new System.Windows.Forms.Label();
            this.ltransporta = new System.Windows.Forms.Label();
            this.bMasUno = new System.Windows.Forms.Button();
            this.bMenosCien = new System.Windows.Forms.Button();
            this.bMasCien = new System.Windows.Forms.Button();
            this.bMenosDiez = new System.Windows.Forms.Button();
            this.bMasDiez = new System.Windows.Forms.Button();
            this.bMenosUno = new System.Windows.Forms.Button();
            this.labeltam = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioMercan = new System.Windows.Forms.RadioButton();
            this.radioPersonas = new System.Windows.Forms.RadioButton();
            this.pTipoBarco = new System.Windows.Forms.PictureBox();
            this.pTipoBarco2 = new System.Windows.Forms.PictureBox();
            this.pTipoBarco3 = new System.Windows.Forms.PictureBox();
            this.pTipoBarco4 = new System.Windows.Forms.PictureBox();
            this.bAceptar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bPrioridad)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pTipoBarco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pTipoBarco2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pTipoBarco3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pTipoBarco4)).BeginInit();
            this.SuspendLayout();
            // 
            // ltitulo
            // 
            this.ltitulo.AutoSize = true;
            this.ltitulo.Font = new System.Drawing.Font("Source Sans Pro Semibold", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltitulo.ForeColor = System.Drawing.Color.DarkRed;
            this.ltitulo.Location = new System.Drawing.Point(150, 23);
            this.ltitulo.Name = "ltitulo";
            this.ltitulo.Size = new System.Drawing.Size(186, 26);
            this.ltitulo.TabIndex = 0;
            this.ltitulo.Text = "Creación de barcos";
            // 
            // lpriori
            // 
            this.lpriori.AutoSize = true;
            this.lpriori.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lpriori.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lpriori.Location = new System.Drawing.Point(35, 276);
            this.lpriori.Name = "lpriori";
            this.lpriori.Size = new System.Drawing.Size(80, 20);
            this.lpriori.TabIndex = 1;
            this.lpriori.Text = "Prioridad: ";
            // 
            // bPrioridad
            // 
            this.bPrioridad.Location = new System.Drawing.Point(121, 276);
            this.bPrioridad.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.bPrioridad.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bPrioridad.Name = "bPrioridad";
            this.bPrioridad.Size = new System.Drawing.Size(65, 20);
            this.bPrioridad.TabIndex = 2;
            this.bPrioridad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ltam
            // 
            this.ltam.AutoSize = true;
            this.ltam.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltam.ForeColor = System.Drawing.SystemColors.Highlight;
            this.ltam.Location = new System.Drawing.Point(30, 182);
            this.ltam.Name = "ltam";
            this.ltam.Size = new System.Drawing.Size(85, 24);
            this.ltam.TabIndex = 4;
            this.ltam.Text = "Tamaño: ";
            // 
            // ltransporta
            // 
            this.ltransporta.AutoSize = true;
            this.ltransporta.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltransporta.ForeColor = System.Drawing.SystemColors.Highlight;
            this.ltransporta.Location = new System.Drawing.Point(30, 90);
            this.ltransporta.Name = "ltransporta";
            this.ltransporta.Size = new System.Drawing.Size(134, 20);
            this.ltransporta.TabIndex = 5;
            this.ltransporta.Text = "¿Qué transporta?: ";
            // 
            // bMasUno
            // 
            this.bMasUno.Location = new System.Drawing.Point(212, 185);
            this.bMasUno.Name = "bMasUno";
            this.bMasUno.Size = new System.Drawing.Size(40, 23);
            this.bMasUno.TabIndex = 7;
            this.bMasUno.Text = "+1";
            this.bMasUno.UseVisualStyleBackColor = true;
            this.bMasUno.Click += new System.EventHandler(this.bMasUno_Click);
            // 
            // bMenosCien
            // 
            this.bMenosCien.Location = new System.Drawing.Point(111, 240);
            this.bMenosCien.Name = "bMenosCien";
            this.bMenosCien.Size = new System.Drawing.Size(40, 23);
            this.bMenosCien.TabIndex = 8;
            this.bMenosCien.Text = "-100";
            this.bMenosCien.UseVisualStyleBackColor = true;
            this.bMenosCien.Click += new System.EventHandler(this.bMenosCien_Click);
            // 
            // bMasCien
            // 
            this.bMasCien.Location = new System.Drawing.Point(212, 243);
            this.bMasCien.Name = "bMasCien";
            this.bMasCien.Size = new System.Drawing.Size(40, 23);
            this.bMasCien.TabIndex = 9;
            this.bMasCien.Text = "+100";
            this.bMasCien.UseVisualStyleBackColor = true;
            this.bMasCien.Click += new System.EventHandler(this.bMasCien_Click);
            // 
            // bMenosDiez
            // 
            this.bMenosDiez.Location = new System.Drawing.Point(111, 211);
            this.bMenosDiez.Name = "bMenosDiez";
            this.bMenosDiez.Size = new System.Drawing.Size(40, 23);
            this.bMenosDiez.TabIndex = 10;
            this.bMenosDiez.Text = "-10";
            this.bMenosDiez.UseVisualStyleBackColor = true;
            this.bMenosDiez.Click += new System.EventHandler(this.bMenosDiez_Click);
            // 
            // bMasDiez
            // 
            this.bMasDiez.Location = new System.Drawing.Point(212, 214);
            this.bMasDiez.Name = "bMasDiez";
            this.bMasDiez.Size = new System.Drawing.Size(40, 23);
            this.bMasDiez.TabIndex = 11;
            this.bMasDiez.Text = "+10";
            this.bMasDiez.UseVisualStyleBackColor = true;
            this.bMasDiez.Click += new System.EventHandler(this.bMasDiez_Click);
            // 
            // bMenosUno
            // 
            this.bMenosUno.Location = new System.Drawing.Point(111, 185);
            this.bMenosUno.Name = "bMenosUno";
            this.bMenosUno.Size = new System.Drawing.Size(40, 23);
            this.bMenosUno.TabIndex = 12;
            this.bMenosUno.Text = "-1";
            this.bMenosUno.UseVisualStyleBackColor = true;
            this.bMenosUno.Click += new System.EventHandler(this.bMenosUno_Click);
            // 
            // labeltam
            // 
            this.labeltam.AutoSize = true;
            this.labeltam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labeltam.Font = new System.Drawing.Font("Source Sans Pro Semibold", 35F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeltam.Location = new System.Drawing.Point(155, 195);
            this.labeltam.Name = "labeltam";
            this.labeltam.Size = new System.Drawing.Size(51, 61);
            this.labeltam.TabIndex = 13;
            this.labeltam.Text = "0";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioMercan);
            this.panel1.Controls.Add(this.radioPersonas);
            this.panel1.Location = new System.Drawing.Point(39, 114);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(125, 65);
            this.panel1.TabIndex = 14;
            // 
            // radioMercan
            // 
            this.radioMercan.AutoSize = true;
            this.radioMercan.Location = new System.Drawing.Point(7, 36);
            this.radioMercan.Name = "radioMercan";
            this.radioMercan.Size = new System.Drawing.Size(75, 17);
            this.radioMercan.TabIndex = 1;
            this.radioMercan.TabStop = true;
            this.radioMercan.Text = "Mercancia";
            this.radioMercan.UseVisualStyleBackColor = true;
            this.radioMercan.CheckedChanged += new System.EventHandler(this.radioMercan_CheckedChanged);
            // 
            // radioPersonas
            // 
            this.radioPersonas.AutoSize = true;
            this.radioPersonas.Checked = true;
            this.radioPersonas.Location = new System.Drawing.Point(7, 12);
            this.radioPersonas.Name = "radioPersonas";
            this.radioPersonas.Size = new System.Drawing.Size(69, 17);
            this.radioPersonas.TabIndex = 0;
            this.radioPersonas.TabStop = true;
            this.radioPersonas.Text = "Personas";
            this.radioPersonas.UseVisualStyleBackColor = true;
            this.radioPersonas.CheckedChanged += new System.EventHandler(this.radioPersonas_CheckedChanged);
            // 
            // pTipoBarco
            // 
            this.pTipoBarco.BackColor = System.Drawing.Color.Transparent;
            this.pTipoBarco.Image = global::Problema2.Properties.Resources.barco3;
            this.pTipoBarco.Location = new System.Drawing.Point(317, 90);
            this.pTipoBarco.Name = "pTipoBarco";
            this.pTipoBarco.Size = new System.Drawing.Size(144, 34);
            this.pTipoBarco.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pTipoBarco.TabIndex = 15;
            this.pTipoBarco.TabStop = false;
            // 
            // pTipoBarco2
            // 
            this.pTipoBarco2.Image = global::Problema2.Properties.Resources.barco1;
            this.pTipoBarco2.Location = new System.Drawing.Point(336, 74);
            this.pTipoBarco2.Name = "pTipoBarco2";
            this.pTipoBarco2.Size = new System.Drawing.Size(100, 93);
            this.pTipoBarco2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pTipoBarco2.TabIndex = 16;
            this.pTipoBarco2.TabStop = false;
            this.pTipoBarco2.Visible = false;
            // 
            // pTipoBarco3
            // 
            this.pTipoBarco3.Image = global::Problema2.Properties.Resources.barco2;
            this.pTipoBarco3.Location = new System.Drawing.Point(336, 62);
            this.pTipoBarco3.Name = "pTipoBarco3";
            this.pTipoBarco3.Size = new System.Drawing.Size(113, 105);
            this.pTipoBarco3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pTipoBarco3.TabIndex = 17;
            this.pTipoBarco3.TabStop = false;
            this.pTipoBarco3.Visible = false;
            // 
            // pTipoBarco4
            // 
            this.pTipoBarco4.Image = global::Problema2.Properties.Resources.barco4;
            this.pTipoBarco4.Location = new System.Drawing.Point(336, 62);
            this.pTipoBarco4.Name = "pTipoBarco4";
            this.pTipoBarco4.Size = new System.Drawing.Size(100, 90);
            this.pTipoBarco4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pTipoBarco4.TabIndex = 18;
            this.pTipoBarco4.TabStop = false;
            this.pTipoBarco4.Visible = false;
            // 
            // bAceptar
            // 
            this.bAceptar.Location = new System.Drawing.Point(374, 294);
            this.bAceptar.Name = "bAceptar";
            this.bAceptar.Size = new System.Drawing.Size(75, 31);
            this.bAceptar.TabIndex = 19;
            this.bAceptar.Text = "Aceptar";
            this.bAceptar.UseVisualStyleBackColor = true;
            this.bAceptar.Click += new System.EventHandler(this.bAceptar_Click);
            // 
            // CreaBarco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 340);
            this.Controls.Add(this.bAceptar);
            this.Controls.Add(this.pTipoBarco4);
            this.Controls.Add(this.pTipoBarco3);
            this.Controls.Add(this.pTipoBarco2);
            this.Controls.Add(this.pTipoBarco);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labeltam);
            this.Controls.Add(this.bMenosUno);
            this.Controls.Add(this.bMasDiez);
            this.Controls.Add(this.bMenosDiez);
            this.Controls.Add(this.bMasCien);
            this.Controls.Add(this.bMenosCien);
            this.Controls.Add(this.bMasUno);
            this.Controls.Add(this.ltransporta);
            this.Controls.Add(this.ltam);
            this.Controls.Add(this.bPrioridad);
            this.Controls.Add(this.lpriori);
            this.Controls.Add(this.ltitulo);
            this.Name = "CreaBarco";
            this.Text = "CreaBarco";
            ((System.ComponentModel.ISupportInitialize)(this.bPrioridad)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pTipoBarco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pTipoBarco2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pTipoBarco3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pTipoBarco4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ltitulo;
        private System.Windows.Forms.Label lpriori;
        private System.Windows.Forms.NumericUpDown bPrioridad;
        private System.Windows.Forms.Label ltam;
        private System.Windows.Forms.Label ltransporta;
        private System.Windows.Forms.Button bMasUno;
        private System.Windows.Forms.Button bMenosCien;
        private System.Windows.Forms.Button bMasCien;
        private System.Windows.Forms.Button bMenosDiez;
        private System.Windows.Forms.Button bMasDiez;
        private System.Windows.Forms.Button bMenosUno;
        private System.Windows.Forms.Label labeltam;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioMercan;
        private System.Windows.Forms.RadioButton radioPersonas;
        private System.Windows.Forms.PictureBox pTipoBarco;
        private System.Windows.Forms.PictureBox pTipoBarco2;
        private System.Windows.Forms.PictureBox pTipoBarco3;
        private System.Windows.Forms.PictureBox pTipoBarco4;
        private System.Windows.Forms.Button bAceptar;
    }
}