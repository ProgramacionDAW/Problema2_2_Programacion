﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      16/02/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Problema2
{
    public partial class CreaBarco : Form
    {
        string numero = "0";
        int prioridad;
        fInicial formulario;
        /// <summary>
        /// Constructor del formulario de Creación de Barcos
        /// </summary>
        /// <param name="formulario">Formulario inicial (mapa)</param>
        public CreaBarco(fInicial formulario)
        {
            InitializeComponent();
            this.formulario = formulario;
        }

        #region Numeros
        /// <summary>
        /// Método que controla el boton +1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bMasUno_Click(object sender, EventArgs e)
        {
            int num = int.Parse(numero);
            num++;
            if (num>500)
            {
                num = 500;
                numero = num.ToString();
                ComprobarBotones(num);
            }
            else
            {
                numero = num.ToString();
                ComprobarBotones(num);
            }
            MostrarBarco();
        }

        /// <summary>
        /// Método que controla el boton +10
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bMasDiez_Click(object sender, EventArgs e)
        {
            int num = int.Parse(numero);
            num+=10;
            if (num>500)
            {
                num = 500;
                numero = num.ToString();
                ComprobarBotones(num);
            }
            else
            {
                numero = num.ToString();
                ComprobarBotones(num);
            }
            MostrarBarco();
        }

        /// <summary>
        /// Método que controla el boton +100
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bMasCien_Click(object sender, EventArgs e)
        {
            int num = int.Parse(numero);
            num += 100;
            if (num>500)
            {
                num = 500;
                numero = num.ToString();
                ComprobarBotones(num);
            }
            else
            {
                numero = num.ToString();
                ComprobarBotones(num);
            }
            MostrarBarco();
        }

        /// <summary>
        /// Método que controla el boton -1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bMenosUno_Click(object sender, EventArgs e)
        {
            int num = int.Parse(numero);
            num--;
            if (num<0)
            {
                num = 0;
                numero = num.ToString();
                ComprobarBotones(num);
            }
            else
            {
                numero = num.ToString();
                ComprobarBotones(num);
            }
            MostrarBarco();
        }

        /// <summary>
        /// Método que controla el boton -10
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bMenosDiez_Click(object sender, EventArgs e)
        {
            int num = int.Parse(numero);
            num-=10;
            if (num < 0)
            {
                num = 0;
                numero = num.ToString();
                ComprobarBotones(num);
            }
            else
            {
                numero = num.ToString();
                ComprobarBotones(num);
            }
            MostrarBarco();
        }

        /// <summary>
        /// Método que controla el boton -100
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bMenosCien_Click(object sender, EventArgs e)
        {
            int num = int.Parse(numero);
            num-=100;
            if (num < 0)
            {
                num = 0;
                numero = num.ToString();
                ComprobarBotones(num);
            }
            else
            {
                numero = num.ToString();
                ComprobarBotones(num);
            }
            MostrarBarco();
        }
        #endregion

        /// <summary>
        /// Método que muestra la imagen del barco correspondiente al tamaño de la eslora y tipo de mercancia introducidos
        /// </summary>
        private void MostrarBarco()
        {
            if (int.Parse(labeltam.Text)<=134 && radioPersonas.Checked)
            {
                pTipoBarco.Visible = true;
                pTipoBarco2.Visible = false;
                pTipoBarco3.Visible = false;
                pTipoBarco4.Visible = false;
            }
            else if(int.Parse(labeltam.Text) >= 135 && radioPersonas.Checked)
            {
                pTipoBarco.Visible = false;
                pTipoBarco2.Visible = true;
                pTipoBarco3.Visible = false;
                pTipoBarco4.Visible = false;
            }
            else if ((int.Parse(labeltam.Text) >= 135 && int.Parse(labeltam.Text) <= 305) && radioMercan.Checked)
            {
                pTipoBarco.Visible = false;
                pTipoBarco2.Visible = false;
                pTipoBarco3.Visible = false;
                pTipoBarco4.Visible = true;
            }
            else if (int.Parse(labeltam.Text) >= 306 && radioMercan.Checked)
            {
                pTipoBarco.Visible = false;
                pTipoBarco2.Visible = false;
                pTipoBarco3.Visible = true;
                pTipoBarco4.Visible = false;
            }
            else
            {
                pTipoBarco.Visible = true;
                pTipoBarco2.Visible = false;
                pTipoBarco3.Visible = false;
                pTipoBarco4.Visible = false;
            }
        }

        /// <summary>
        /// Método que hace que los botones de tamaño (+1,+10,+100) se ajusten a la ventana
        /// </summary>
        /// <param name="num"></param>
        private void ComprobarBotones(int num)
        {
            if (num <= 9)
            {
                Point pos1 = new Point(212, bMasUno.Location.Y);
                bMasUno.Location = pos1;
                Point pos2 = new Point(212, bMasDiez.Location.Y);
                bMasDiez.Location = pos2;
                Point pos3 = new Point(212, bMasCien.Location.Y);
                bMasCien.Location = pos3;
                labeltam.Text = numero;
            }
            else if (num > 9 && num <= 99)
            {
                Point pos1 = new Point(236, bMasUno.Location.Y);
                bMasUno.Location = pos1;
                Point pos2 = new Point(236, bMasDiez.Location.Y);
                bMasDiez.Location = pos2;
                Point pos3 = new Point(236, bMasCien.Location.Y);
                bMasCien.Location = pos3;
                labeltam.Text = numero;
            }
            else if (num > 99 && num <= 999)
            {
                Point pos1 = new Point(260, bMasUno.Location.Y);
                bMasUno.Location = pos1;
                Point pos2 = new Point(260, bMasDiez.Location.Y);
                bMasDiez.Location = pos2;
                Point pos3 = new Point(260, bMasCien.Location.Y);
                bMasCien.Location = pos3;
                labeltam.Text = numero;
            }
            else if (num > 999 && num <= 9999)
            {
                Point pos1 = new Point(284, bMasUno.Location.Y);
                bMasUno.Location = pos1;
                Point pos2 = new Point(284, bMasDiez.Location.Y);
                bMasDiez.Location = pos2;
                Point pos3 = new Point(284, bMasCien.Location.Y);
                bMasCien.Location = pos3;
                labeltam.Text = numero;
            }
            else
            {
                labeltam.Text = labeltam.Text;
            }
        }

        /// <summary>
        /// Método que controla el radio button de Personas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioPersonas_CheckedChanged(object sender, EventArgs e)
        {
            MostrarBarco();
        }

        /// <summary>
        /// Método que controla el radio button de Mercancia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioMercan_CheckedChanged(object sender, EventArgs e)
        {
            MostrarBarco();
        }

        /// <summary>
        /// Método que controla el boton aceptar. Genera un barco según las especificaciones introducidas y cierra el formulario.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bAceptar_Click(object sender, EventArgs e)
        {
            prioridad = (int)bPrioridad.Value;
            bool encontrado = false;
            Point punto = new Point(-500, 0);
            if (int.Parse(labeltam.Text) <= 134 && radioPersonas.Checked)
            {
                for (int i=0;i<6;i++)
                {
                    Panel barco = formulario.Barcos[i];
                    if (barco.Visible==false)
                    {
                        Barco nBarco = new Barco("Ligeras", 1, prioridad, int.Parse(numero), barco.Name.ToString());
                        Program.fase1.AgregarBarco(nBarco);
                        barco.Visible = true;
                        barco.Location = punto;
                        encontrado = true;
                        foreach (Control control in barco.Controls)
                        {
                            if (control is Label)
                            {
                                control.Text = string.Concat("Prioridad: ", prioridad.ToString());
                            }
                        }
                        break;
                    }
                }
            }
            else if (int.Parse(labeltam.Text) >= 135 && radioPersonas.Checked)
            {
                for (int i = 6; i < 12; i++)
                {
                    Panel barco = formulario.Barcos[i];
                    if (barco.Visible == false)
                    {
                        Barco nBarco = new Barco("Crucero", 2, prioridad, int.Parse(numero), barco.Name.ToString());
                        Program.fase1.AgregarBarco(nBarco);
                        barco.Visible = true;
                        barco.Location = punto;
                        encontrado = true;
                        foreach (Control control in barco.Controls)
                        {
                            if (control is Label)
                            {
                                control.Text = string.Concat("Prioridad: ", prioridad.ToString());
                            }
                        }
                        break;
                    }
                }
            }
            else if ((int.Parse(labeltam.Text) >= 135 && int.Parse(labeltam.Text) <= 305) && radioMercan.Checked)
            {
                for (int i = 12; i < 18; i++)
                {
                    Panel barco = formulario.Barcos[i];
                    if (barco.Visible == false)
                    {
                        Barco nBarco = new Barco("Mercantes", 3, prioridad, int.Parse(numero), barco.Name.ToString());
                        Program.fase1.AgregarBarco(nBarco);
                        barco.Visible = true;
                        barco.Location = punto;
                        encontrado = true;
                        foreach (Control control in barco.Controls)
                        {
                            if (control is Label)
                            {
                                control.Text = string.Concat("Prioridad: ", prioridad.ToString());
                            }
                        }
                        break;
                    }
                }
            }
            else if (int.Parse(labeltam.Text) >= 306 && radioMercan.Checked)
            {
                for (int i = 18; i < formulario.Barcos.Count; i++)
                {
                    Panel barco = formulario.Barcos[i];
                    if (barco.Visible == false)
                    {
                        Barco nBarco = new Barco("MercantesGiga", 4, prioridad, int.Parse(numero), barco.Name.ToString());
                        Program.fase1.AgregarBarco(nBarco);
                        barco.Visible = true;
                        barco.Location = punto;
                        encontrado = true;
                        foreach (Control control in barco.Controls)
                        {
                            if (control is Label)
                            {
                                control.Text = string.Concat("Prioridad: ", prioridad.ToString());
                            }
                        }
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < 6; i++)
                {
                    Panel barco = formulario.Barcos[i];
                    if (barco.Visible == false)
                    {
                        Barco nBarco = new Barco("Ligeras", 1, prioridad, int.Parse(numero), barco.Name.ToString());
                        Program.fase1.AgregarBarco(nBarco);
                        barco.Visible = true;
                        barco.Location = punto;
                        encontrado = true;
                        foreach (Control control in barco.Controls)
                        {
                            if (control is Label)
                            {
                                control.Text = string.Concat("Prioridad: ", prioridad.ToString());
                            }
                        }
                        break;
                    }
                }
            }
            if (encontrado)
            {
                MessageBox.Show("Barco agregado con exito.", "Confirmación", MessageBoxButtons.OK);
                this.Close();
            }
            else
            {
                MessageBox.Show("Se ha alcanzado el tope de barcos creados de este tipo.", "Error", MessageBoxButtons.OK);
                this.Close();
            }
        }
    }
}
