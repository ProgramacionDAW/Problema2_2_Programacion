﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      16/02/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema2
{
    class Fase1
    {
        #region Atributos
        List<Barco> colaA = new List<Barco>();
        List<Barco> colaB = new List<Barco>();
        List<Barco> colaC = new List<Barco>();
        List<Barco> colaD = new List<Barco>();
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor base de la clase Fase1
        /// </summary>
        public Fase1()
        {

        }
        #endregion

        #region Metodos
        /// <summary>
        /// Método que agrega un barco a la Fase1 dependiendo del tipo de barco
        /// </summary>
        /// <param name="barco"></param>
        public void AgregarBarco(Barco barco)
        {
            switch (barco.Tipo)
            {
                case "Ligeras":
                    colaA.Add(barco);
                    colaA = colaA.OrderByDescending(elemento => elemento.Prioridad).ToList();
                    break;
                case "Mercantes":
                    colaB.Add(barco);
                    colaB = colaB.OrderByDescending(elemento => elemento.Prioridad).ToList();
                    break;
                case "MercantesGiga":
                    colaC.Add(barco);
                    colaC = colaC.OrderByDescending(elemento => elemento.Prioridad).ToList();
                    break;
                case "Crucero":
                    colaD.Add(barco);
                    colaD = colaD.OrderByDescending(elemento => elemento.Prioridad).ToList();
                    break;
            }
        }

        /// <summary>
        /// Método que mueve un barco de la Fase1 a la Fase2 si esta disponible
        /// </summary>
        /// <param name="letra"></param>
        /// <returns></returns>
        public Barco SiguienteFase(char letra)
        {
            if (Program.fase2.Disponible==false)
            {
                return null;
            }
            else
            {
                Barco barco;
                switch (letra)
                {
                    case 'A':
                        if (colaA.Count > 0)
                        {
                            barco = colaA[0];
                            Program.fase2.AgregarBarco(barco);
                            colaA.Remove(barco);
                            return barco;
                        }
                        break;
                    case 'B':
                        if (colaB.Count > 0)
                        {
                            barco = colaB[0];
                            Program.fase2.AgregarBarco(barco);
                            colaB.Remove(barco);
                            return barco;
                        }
                        break;
                    case 'C':
                        if (colaC.Count > 0)
                        {
                            barco = colaC[0];
                            Program.fase2.AgregarBarco(barco);
                            colaC.Remove(barco);
                            return barco;
                        }
                        break;
                    case 'D':
                        if (colaD.Count > 0)
                        {
                            barco = colaD[0];
                            Program.fase2.AgregarBarco(barco);
                            colaD.Remove(barco);
                            return barco;
                        }
                        break;
                }
            }
            return null;
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve la cantidad de barcos que hay en la cola A
        /// </summary>
        public int ColaA
        {
            get
            {
                return colaA.Count;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la cantidad de barcos que hay en la cola B
        /// </summary>
        public int ColaB
        {
            get
            {
                return colaB.Count;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la cantidad de barcos que hay en la cola C
        /// </summary>
        public int ColaC
        {
            get
            {
                return colaC.Count;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la cantidad de barcos que hay en la cola D
        /// </summary>
        public int ColaD
        {
            get
            {
                return colaD.Count;
            }
        }
        #endregion
    }
}
