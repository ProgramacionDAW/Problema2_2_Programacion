﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      16/02/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema2
{
    class Fase2
    {
        #region Atributos
        List<Barco> barcos = new List<Barco>();
        bool disponible = true;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor de la Fase2
        /// </summary>
        public Fase2()
        {

        }
        #endregion

        #region Metodos
        /// <summary>
        /// Método que agrega un barco a la Fase2
        /// </summary>
        /// <param name="barco"></param>
        public void AgregarBarco(Barco barco)
        {
            barcos.Add(barco);
            disponible = false;
        }

        /// <summary>
        /// Método que mueve un barco de la Fase2 a la Fase3 según su eslora
        /// </summary>
        public void SiguienteFase()
        {
            Barco barco=barcos[0];
            if (barco.Eslora<=250)
            {
                if (Program.fase3.Disponible == true)
                {
                    Program.fase3.AgregarBarco(barco);
                    barcos.Remove(barco);
                    disponible = true;
                }
            }
            else
            {
                if (Program.fase3.Disponible2 == true)
                {
                    Program.fase3.AgregarBarco(barco);
                    barcos.Remove(barco);
                    disponible = true;
                }
            }
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve el total de barcos que hay en la fase 2
        /// </summary>
        public int Total
        {
            get
            {
                return barcos.Count;
            }
        }

        /// <summary>
        /// Propiedad que devuelve si la fase 2 admite o no más barcos
        /// </summary>
        public bool Disponible
        {
            get
            {
                return disponible;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la lista de barcos que hay en la fase 2
        /// </summary>
        public List<Barco> Barcos
        {
            get
            {
                return barcos;
            }
        }
        #endregion
    }
}
