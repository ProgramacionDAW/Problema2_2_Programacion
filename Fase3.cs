﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      16/02/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema2
{
    class Fase3
    {
        #region Atributos
        List<Barco> barcos = new List<Barco>();
        bool disponible = true;
        bool disponible2 = true;
        #endregion
        #region Constructores
        /// <summary>
        /// Constructor de la Fase 3
        /// </summary>
        public Fase3()
        {

        }
        #endregion

        #region Metodos
        /// <summary>
        /// Método que agrega un barco a la fase 3
        /// </summary>
        /// <param name="barco"></param>
        public void AgregarBarco(Barco barco)
        {
            barcos.Add(barco);
            if (barco.Eslora<=250)
            {
                if (BarcosRapido>=3)
                {
                    disponible = false;
                }
            }
            else
            {
                if (BarcosLento >= 3)
                {
                    disponible2 = false;
                }
            }
        }

        /// <summary>
        /// Método que mueve un barco de la fase 3 a la fase 4 si esta disponible
        /// </summary>
        /// <param name="barco"></param>
        public void SiguienteFase(Barco barco)
        {
            if (Program.fase4.Disponible == true)
            {
                Program.fase4.AgregarBarco(barco);
                barcos.Remove(barco);
                if (barco.Eslora <= 250)
                {
                    disponible = true;
                }
                else
                {
                    disponible2 = true;
                }
            }
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve el total de barcos que hay en la fase 3
        /// </summary>
        public int Total
        {
            get
            {
                return barcos.Count;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la disponibilidad de la via rápida
        /// </summary>
        public bool Disponible
        {
            get
            {
                return disponible;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la disponibilidad de la via lenta
        /// </summary>
        public bool Disponible2
        {
            get
            {
                return disponible2;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la lista de barcos que hay en la fase 3
        /// </summary>
        public List<Barco> Barcos
        {
            get
            {
                return barcos;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la cantidad de barcos que hay en la via rápida
        /// </summary>
        public int BarcosRapido
        {
            get
            {
                int contador = 0;
                for (int i=0;i<barcos.Count;i++)
                {
                    Barco barco = barcos[i];
                    if (barco.Eslora <= 250)
                    {
                        contador++;
                    }
                }
                return contador;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la cantidad de barcos que hay en la via lenta
        /// </summary>
        public int BarcosLento
        {
            get
            {
                int contador = 0;
                for (int i = 0; i < barcos.Count; i++)
                {
                    Barco barco = barcos[i];
                    if (barco.Eslora > 250)
                    {
                        contador++;
                    }
                }
                return contador;
            }
        }
        #endregion
    }
}
