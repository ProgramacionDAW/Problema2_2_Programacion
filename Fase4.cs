﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      16/02/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema2
{
    class Fase4
    {
        #region Atributos
        List<Barco> barcos = new List<Barco>();
        bool disponible = true;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor de la fase 4
        /// </summary>
        public Fase4()
        {

        }
        #endregion

        #region Metodos
        /// <summary>
        /// Método que agrega un barco a la fase 4
        /// </summary>
        /// <param name="barco"></param>
        public void AgregarBarco(Barco barco)
        {
            barcos.Add(barco);
            if (barcos.Count >= 2)
            {
                disponible = false;
            }
        }

        /// <summary>
        /// Método que elimina un barco de la fase 4
        /// </summary>
        /// <param name="barco"></param>
        public void EliminarBarco(Barco barco)
        {
            barcos.Remove(barco);
            disponible = true;
        }

        /// <summary>
        /// Método que devuelve el barco a mover en caso de que haya 2 barcos intentando salir a la vez
        /// </summary>
        /// <returns></returns>
        public Barco ObtenerBarco()
        {
            if (barcos[0].Prioridad!=barcos[1].Prioridad)
            {
                if (barcos[0].Prioridad > barcos[1].Prioridad)
                {
                    return barcos[0];
                }
                else
                {
                    return barcos[1];
                }
            }
            else
            {
                if (barcos[0].Tipo== "Ligeras" || barcos[0].Tipo == "Mercantes")
                {
                    return barcos[0];
                }
                else
                {
                    return barcos[1];
                }
            }
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve el total de barcos que hay en la fase 4
        /// </summary>
        public int Total
        {
            get
            {
                return barcos.Count;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la disponibilidad de la fase 4
        /// </summary>
        public bool Disponible
        {
            get
            {
                return disponible;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la lista de barcos que hay en la fase 4
        /// </summary>
        public List<Barco> Barcos
        {
            get
            {
                return barcos;
            }
        }
        #endregion
    }
}
