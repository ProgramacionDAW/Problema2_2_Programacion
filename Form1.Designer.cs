﻿namespace Problema2
{
    partial class fInicial
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fInicial));
            this.flecha1 = new System.Windows.Forms.PictureBox();
            this.flecha2 = new System.Windows.Forms.PictureBox();
            this.flecha3 = new System.Windows.Forms.PictureBox();
            this.flecha4 = new System.Windows.Forms.PictureBox();
            this.creaBarco = new System.Windows.Forms.PictureBox();
            this.leyenda = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pBarco1 = new System.Windows.Forms.PictureBox();
            this.pBarco2 = new System.Windows.Forms.PictureBox();
            this.pBarco4 = new System.Windows.Forms.PictureBox();
            this.verPosiciones = new System.Windows.Forms.PictureBox();
            this.pBarco2A = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lContador = new System.Windows.Forms.Label();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.lContador2 = new System.Windows.Forms.Label();
            this.cantidadA = new System.Windows.Forms.Label();
            this.cantidadD = new System.Windows.Forms.Label();
            this.CantidadB = new System.Windows.Forms.Label();
            this.CantidadC = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pBarco3 = new System.Windows.Forms.PictureBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.flecha1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flecha2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flecha3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flecha4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creaBarco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leyenda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.verPosiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco2A)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco3)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            this.SuspendLayout();
            // 
            // flecha1
            // 
            this.flecha1.BackColor = System.Drawing.Color.Transparent;
            this.flecha1.Image = global::Problema2.Properties.Resources.f1OFF;
            this.flecha1.Location = new System.Drawing.Point(157, 23);
            this.flecha1.Name = "flecha1";
            this.flecha1.Size = new System.Drawing.Size(82, 88);
            this.flecha1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.flecha1.TabIndex = 0;
            this.flecha1.TabStop = false;
            this.flecha1.Click += new System.EventHandler(this.MoverBarcosA);
            this.flecha1.MouseEnter += new System.EventHandler(this.flecha1_MouseEnter);
            this.flecha1.MouseLeave += new System.EventHandler(this.flecha1_MouseLeave);
            // 
            // flecha2
            // 
            this.flecha2.BackColor = System.Drawing.Color.Transparent;
            this.flecha2.Image = ((System.Drawing.Image)(resources.GetObject("flecha2.Image")));
            this.flecha2.Location = new System.Drawing.Point(157, 153);
            this.flecha2.Name = "flecha2";
            this.flecha2.Size = new System.Drawing.Size(103, 42);
            this.flecha2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.flecha2.TabIndex = 1;
            this.flecha2.TabStop = false;
            this.flecha2.Click += new System.EventHandler(this.MoverBarcosD);
            this.flecha2.MouseEnter += new System.EventHandler(this.flecha2_MouseEnter);
            this.flecha2.MouseLeave += new System.EventHandler(this.flecha2_MouseLeave);
            // 
            // flecha3
            // 
            this.flecha3.BackColor = System.Drawing.Color.Transparent;
            this.flecha3.Image = ((System.Drawing.Image)(resources.GetObject("flecha3.Image")));
            this.flecha3.Location = new System.Drawing.Point(157, 268);
            this.flecha3.Name = "flecha3";
            this.flecha3.Size = new System.Drawing.Size(103, 42);
            this.flecha3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.flecha3.TabIndex = 2;
            this.flecha3.TabStop = false;
            this.flecha3.Click += new System.EventHandler(this.MoverBarcosB);
            this.flecha3.MouseEnter += new System.EventHandler(this.flecha3_MouseEnter);
            this.flecha3.MouseLeave += new System.EventHandler(this.flecha3_MouseLeave);
            // 
            // flecha4
            // 
            this.flecha4.BackColor = System.Drawing.Color.Transparent;
            this.flecha4.Image = global::Problema2.Properties.Resources.f4OFF;
            this.flecha4.Location = new System.Drawing.Point(157, 349);
            this.flecha4.Name = "flecha4";
            this.flecha4.Size = new System.Drawing.Size(59, 91);
            this.flecha4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.flecha4.TabIndex = 3;
            this.flecha4.TabStop = false;
            this.flecha4.Click += new System.EventHandler(this.MoverBarcosC);
            this.flecha4.MouseEnter += new System.EventHandler(this.flecha4_MouseEnter);
            this.flecha4.MouseLeave += new System.EventHandler(this.flecha4_MouseLeave);
            // 
            // creaBarco
            // 
            this.creaBarco.BackColor = System.Drawing.Color.Transparent;
            this.creaBarco.Image = global::Problema2.Properties.Resources.creabarcoOFF;
            this.creaBarco.Location = new System.Drawing.Point(291, 332);
            this.creaBarco.Name = "creaBarco";
            this.creaBarco.Size = new System.Drawing.Size(176, 75);
            this.creaBarco.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.creaBarco.TabIndex = 4;
            this.creaBarco.TabStop = false;
            this.creaBarco.Click += new System.EventHandler(this.creaBarco_Click);
            this.creaBarco.MouseEnter += new System.EventHandler(this.creaBarco_MouseEnter);
            this.creaBarco.MouseLeave += new System.EventHandler(this.creaBarco_MouseLeave);
            // 
            // leyenda
            // 
            this.leyenda.BackColor = System.Drawing.Color.Transparent;
            this.leyenda.Image = global::Problema2.Properties.Resources.leyendaOFF;
            this.leyenda.Location = new System.Drawing.Point(291, 413);
            this.leyenda.Name = "leyenda";
            this.leyenda.Size = new System.Drawing.Size(176, 75);
            this.leyenda.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.leyenda.TabIndex = 5;
            this.leyenda.TabStop = false;
            this.leyenda.Click += new System.EventHandler(this.leyenda_Click);
            this.leyenda.MouseEnter += new System.EventHandler(this.leyenda_MouseEnter);
            this.leyenda.MouseLeave += new System.EventHandler(this.leyenda_MouseLeave);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pBarco1
            // 
            this.pBarco1.BackColor = System.Drawing.Color.Transparent;
            this.pBarco1.Image = global::Problema2.Properties.Resources.barco3;
            this.pBarco1.Location = new System.Drawing.Point(12, 40);
            this.pBarco1.Name = "pBarco1";
            this.pBarco1.Size = new System.Drawing.Size(121, 33);
            this.pBarco1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBarco1.TabIndex = 6;
            this.pBarco1.TabStop = false;
            // 
            // pBarco2
            // 
            this.pBarco2.BackColor = System.Drawing.Color.Transparent;
            this.pBarco2.Image = global::Problema2.Properties.Resources.barco4;
            this.pBarco2.Location = new System.Drawing.Point(12, 245);
            this.pBarco2.Name = "pBarco2";
            this.pBarco2.Size = new System.Drawing.Size(95, 83);
            this.pBarco2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBarco2.TabIndex = 7;
            this.pBarco2.TabStop = false;
            // 
            // pBarco4
            // 
            this.pBarco4.BackColor = System.Drawing.Color.Transparent;
            this.pBarco4.Image = global::Problema2.Properties.Resources.barco2;
            this.pBarco4.Location = new System.Drawing.Point(12, 349);
            this.pBarco4.Name = "pBarco4";
            this.pBarco4.Size = new System.Drawing.Size(95, 91);
            this.pBarco4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBarco4.TabIndex = 9;
            this.pBarco4.TabStop = false;
            // 
            // verPosiciones
            // 
            this.verPosiciones.BackColor = System.Drawing.Color.Transparent;
            this.verPosiciones.Image = global::Problema2.Properties.Resources.posicionesOFF;
            this.verPosiciones.Location = new System.Drawing.Point(935, 386);
            this.verPosiciones.Name = "verPosiciones";
            this.verPosiciones.Size = new System.Drawing.Size(166, 64);
            this.verPosiciones.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.verPosiciones.TabIndex = 10;
            this.verPosiciones.TabStop = false;
            this.verPosiciones.Click += new System.EventHandler(this.verPosiciones_Click);
            this.verPosiciones.MouseEnter += new System.EventHandler(this.verPosiciones_MouseEnter);
            this.verPosiciones.MouseLeave += new System.EventHandler(this.verPosiciones_MouseLeave);
            // 
            // pBarco2A
            // 
            this.pBarco2A.BackColor = System.Drawing.Color.Transparent;
            this.pBarco2A.Image = global::Problema2.Properties.Resources.barco1;
            this.pBarco2A.Location = new System.Drawing.Point(3, 3);
            this.pBarco2A.Name = "pBarco2A";
            this.pBarco2A.Size = new System.Drawing.Size(95, 87);
            this.pBarco2A.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBarco2A.TabIndex = 22;
            this.pBarco2A.TabStop = false;
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lContador
            // 
            this.lContador.AutoSize = true;
            this.lContador.BackColor = System.Drawing.Color.Transparent;
            this.lContador.Font = new System.Drawing.Font("Source Sans Pro", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lContador.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lContador.Location = new System.Drawing.Point(327, 23);
            this.lContador.Name = "lContador";
            this.lContador.Size = new System.Drawing.Size(152, 34);
            this.lContador.TabIndex = 35;
            this.lContador.Text = "Contador: 0";
            // 
            // timer3
            // 
            this.timer3.Interval = 1;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(935, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 23);
            this.button1.TabIndex = 36;
            this.button1.Text = "Mismo Tiempo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.MismoTiempo);
            // 
            // lContador2
            // 
            this.lContador2.AutoSize = true;
            this.lContador2.BackColor = System.Drawing.Color.Transparent;
            this.lContador2.Font = new System.Drawing.Font("Source Sans Pro", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lContador2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lContador2.Location = new System.Drawing.Point(492, 444);
            this.lContador2.Name = "lContador2";
            this.lContador2.Size = new System.Drawing.Size(152, 34);
            this.lContador2.TabIndex = 37;
            this.lContador2.Text = "Contador: 0";
            // 
            // cantidadA
            // 
            this.cantidadA.AutoSize = true;
            this.cantidadA.BackColor = System.Drawing.Color.Transparent;
            this.cantidadA.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cantidadA.ForeColor = System.Drawing.Color.DarkRed;
            this.cantidadA.Location = new System.Drawing.Point(116, 17);
            this.cantidadA.Name = "cantidadA";
            this.cantidadA.Size = new System.Drawing.Size(17, 20);
            this.cantidadA.TabIndex = 38;
            this.cantidadA.Text = "0";
            // 
            // cantidadD
            // 
            this.cantidadD.AutoSize = true;
            this.cantidadD.BackColor = System.Drawing.Color.Transparent;
            this.cantidadD.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cantidadD.ForeColor = System.Drawing.Color.DarkRed;
            this.cantidadD.Location = new System.Drawing.Point(116, 129);
            this.cantidadD.Name = "cantidadD";
            this.cantidadD.Size = new System.Drawing.Size(17, 20);
            this.cantidadD.TabIndex = 39;
            this.cantidadD.Text = "0";
            // 
            // CantidadB
            // 
            this.CantidadB.AutoSize = true;
            this.CantidadB.BackColor = System.Drawing.Color.Transparent;
            this.CantidadB.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CantidadB.ForeColor = System.Drawing.Color.DarkRed;
            this.CantidadB.Location = new System.Drawing.Point(116, 245);
            this.CantidadB.Name = "CantidadB";
            this.CantidadB.Size = new System.Drawing.Size(17, 20);
            this.CantidadB.TabIndex = 40;
            this.CantidadB.Text = "0";
            // 
            // CantidadC
            // 
            this.CantidadC.AutoSize = true;
            this.CantidadC.BackColor = System.Drawing.Color.Transparent;
            this.CantidadC.Font = new System.Drawing.Font("Source Sans Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CantidadC.ForeColor = System.Drawing.Color.DarkRed;
            this.CantidadC.Location = new System.Drawing.Point(116, 349);
            this.CantidadC.Name = "CantidadC";
            this.CantidadC.Size = new System.Drawing.Size(17, 20);
            this.CantidadC.TabIndex = 41;
            this.CantidadC.Text = "0";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Location = new System.Drawing.Point(1106, 413);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(134, 70);
            this.panel4.TabIndex = 44;
            this.panel4.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 24);
            this.label7.TabIndex = 18;
            this.label7.Text = "Prioridad: ";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::Problema2.Properties.Resources.barco3;
            this.pictureBox3.Location = new System.Drawing.Point(3, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(121, 33);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 16;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(1107, 337);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(134, 70);
            this.panel1.TabIndex = 45;
            this.panel1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 24);
            this.label1.TabIndex = 18;
            this.label1.Text = "Prioridad: ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Problema2.Properties.Resources.barco3;
            this.pictureBox1.Location = new System.Drawing.Point(3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(121, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Location = new System.Drawing.Point(1106, 258);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(134, 70);
            this.panel2.TabIndex = 45;
            this.panel2.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 24);
            this.label2.TabIndex = 18;
            this.label2.Text = "Prioridad: ";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Problema2.Properties.Resources.barco3;
            this.pictureBox2.Location = new System.Drawing.Point(3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(121, 33);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.pictureBox4);
            this.panel3.Location = new System.Drawing.Point(1106, 175);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(134, 70);
            this.panel3.TabIndex = 45;
            this.panel3.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 24);
            this.label3.TabIndex = 18;
            this.label3.Text = "Prioridad: ";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::Problema2.Properties.Resources.barco3;
            this.pictureBox4.Location = new System.Drawing.Point(3, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(121, 33);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 16;
            this.pictureBox4.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.pictureBox5);
            this.panel5.Location = new System.Drawing.Point(1106, 99);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(134, 70);
            this.panel5.TabIndex = 45;
            this.panel5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 24);
            this.label4.TabIndex = 18;
            this.label4.Text = "Prioridad: ";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Image = global::Problema2.Properties.Resources.barco3;
            this.pictureBox5.Location = new System.Drawing.Point(3, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(121, 33);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 16;
            this.pictureBox5.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.pictureBox6);
            this.panel6.Location = new System.Drawing.Point(1103, 12);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(134, 70);
            this.panel6.TabIndex = 45;
            this.panel6.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 24);
            this.label5.TabIndex = 18;
            this.label5.Text = "Prioridad: ";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Image = global::Problema2.Properties.Resources.barco3;
            this.pictureBox6.Location = new System.Drawing.Point(3, 4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(121, 33);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 16;
            this.pictureBox6.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.pBarco2A);
            this.panel7.Location = new System.Drawing.Point(786, 12);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(123, 121);
            this.panel7.TabIndex = 46;
            this.panel7.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 24);
            this.label6.TabIndex = 19;
            this.label6.Text = "Prioridad: ";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.pictureBox7);
            this.panel8.Location = new System.Drawing.Point(786, 139);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(123, 121);
            this.panel8.TabIndex = 47;
            this.panel8.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 24);
            this.label8.TabIndex = 19;
            this.label8.Text = "Prioridad: ";
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.Image = global::Problema2.Properties.Resources.barco1;
            this.pictureBox7.Location = new System.Drawing.Point(3, 3);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(95, 87);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 22;
            this.pictureBox7.TabStop = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Transparent;
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.pictureBox8);
            this.panel9.Location = new System.Drawing.Point(789, 268);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(123, 121);
            this.panel9.TabIndex = 47;
            this.panel9.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 24);
            this.label9.TabIndex = 19;
            this.label9.Text = "Prioridad: ";
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Image = global::Problema2.Properties.Resources.barco1;
            this.pictureBox8.Location = new System.Drawing.Point(3, 3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(95, 87);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 22;
            this.pictureBox8.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.pictureBox9);
            this.panel10.Location = new System.Drawing.Point(915, 52);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(123, 121);
            this.panel10.TabIndex = 47;
            this.panel10.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 24);
            this.label10.TabIndex = 19;
            this.label10.Text = "Prioridad: ";
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.Image = global::Problema2.Properties.Resources.barco1;
            this.pictureBox9.Location = new System.Drawing.Point(3, 3);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(95, 87);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 22;
            this.pictureBox9.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.Controls.Add(this.label11);
            this.panel11.Controls.Add(this.pictureBox10);
            this.panel11.Location = new System.Drawing.Point(915, 179);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(123, 121);
            this.panel11.TabIndex = 47;
            this.panel11.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 24);
            this.label11.TabIndex = 19;
            this.label11.Text = "Prioridad: ";
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.Image = global::Problema2.Properties.Resources.barco1;
            this.pictureBox10.Location = new System.Drawing.Point(3, 3);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(95, 87);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 22;
            this.pictureBox10.TabStop = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Transparent;
            this.panel12.Controls.Add(this.label12);
            this.panel12.Controls.Add(this.pictureBox11);
            this.panel12.Location = new System.Drawing.Point(660, 268);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(123, 121);
            this.panel12.TabIndex = 47;
            this.panel12.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 24);
            this.label12.TabIndex = 19;
            this.label12.Text = "Prioridad: ";
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.Image = global::Problema2.Properties.Resources.barco1;
            this.pictureBox11.Location = new System.Drawing.Point(3, 3);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(95, 87);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 22;
            this.pictureBox11.TabStop = false;
            // 
            // pBarco3
            // 
            this.pBarco3.BackColor = System.Drawing.Color.Transparent;
            this.pBarco3.Image = global::Problema2.Properties.Resources.barco1;
            this.pBarco3.Location = new System.Drawing.Point(12, 129);
            this.pBarco3.Name = "pBarco3";
            this.pBarco3.Size = new System.Drawing.Size(95, 87);
            this.pBarco3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBarco3.TabIndex = 8;
            this.pBarco3.TabStop = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Transparent;
            this.panel13.Controls.Add(this.label13);
            this.panel13.Controls.Add(this.pictureBox12);
            this.panel13.Location = new System.Drawing.Point(523, 13);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(123, 120);
            this.panel13.TabIndex = 48;
            this.panel13.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 24);
            this.label13.TabIndex = 23;
            this.label13.Text = "Prioridad: ";
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox12.Image = global::Problema2.Properties.Resources.barco4;
            this.pictureBox12.Location = new System.Drawing.Point(3, 3);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(95, 83);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 49;
            this.pictureBox12.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Transparent;
            this.panel14.Controls.Add(this.label14);
            this.panel14.Controls.Add(this.pictureBox13);
            this.panel14.Location = new System.Drawing.Point(523, 142);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(123, 120);
            this.panel14.TabIndex = 50;
            this.panel14.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 24);
            this.label14.TabIndex = 23;
            this.label14.Text = "Prioridad: ";
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox13.Image = global::Problema2.Properties.Resources.barco4;
            this.pictureBox13.Location = new System.Drawing.Point(3, 3);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(95, 83);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 49;
            this.pictureBox13.TabStop = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Transparent;
            this.panel15.Controls.Add(this.label15);
            this.panel15.Controls.Add(this.pictureBox14);
            this.panel15.Location = new System.Drawing.Point(523, 268);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(123, 120);
            this.panel15.TabIndex = 50;
            this.panel15.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 24);
            this.label15.TabIndex = 23;
            this.label15.Text = "Prioridad: ";
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.Image = global::Problema2.Properties.Resources.barco4;
            this.pictureBox14.Location = new System.Drawing.Point(3, 3);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(95, 83);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 49;
            this.pictureBox14.TabStop = false;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.label16);
            this.panel16.Controls.Add(this.pictureBox15);
            this.panel16.Location = new System.Drawing.Point(394, 145);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(123, 120);
            this.panel16.TabIndex = 51;
            this.panel16.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 90);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 24);
            this.label16.TabIndex = 23;
            this.label16.Text = "Prioridad: ";
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox15.Image = global::Problema2.Properties.Resources.barco4;
            this.pictureBox15.Location = new System.Drawing.Point(3, 3);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(95, 83);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 49;
            this.pictureBox15.TabStop = false;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Transparent;
            this.panel17.Controls.Add(this.label17);
            this.panel17.Controls.Add(this.pictureBox16);
            this.panel17.Location = new System.Drawing.Point(652, 140);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(123, 120);
            this.panel17.TabIndex = 50;
            this.panel17.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(3, 90);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 24);
            this.label17.TabIndex = 23;
            this.label17.Text = "Prioridad: ";
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox16.Image = global::Problema2.Properties.Resources.barco4;
            this.pictureBox16.Location = new System.Drawing.Point(3, 3);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(95, 83);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 49;
            this.pictureBox16.TabStop = false;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Transparent;
            this.panel18.Controls.Add(this.label18);
            this.panel18.Controls.Add(this.pictureBox17);
            this.panel18.Location = new System.Drawing.Point(655, 14);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(123, 120);
            this.panel18.TabIndex = 50;
            this.panel18.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 90);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(96, 24);
            this.label18.TabIndex = 23;
            this.label18.Text = "Prioridad: ";
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox17.Image = global::Problema2.Properties.Resources.barco4;
            this.pictureBox17.Location = new System.Drawing.Point(3, 3);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(95, 83);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 49;
            this.pictureBox17.TabStop = false;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Transparent;
            this.panel19.Controls.Add(this.label19);
            this.panel19.Controls.Add(this.pictureBox18);
            this.panel19.Location = new System.Drawing.Point(245, 6);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(111, 130);
            this.panel19.TabIndex = 52;
            this.panel19.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 97);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(96, 24);
            this.label19.TabIndex = 50;
            this.label19.Text = "Prioridad: ";
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox18.Image = global::Problema2.Properties.Resources.barco2;
            this.pictureBox18.Location = new System.Drawing.Point(3, 3);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(95, 91);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 53;
            this.pictureBox18.TabStop = false;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Transparent;
            this.panel20.Controls.Add(this.label20);
            this.panel20.Controls.Add(this.pictureBox19);
            this.panel20.Location = new System.Drawing.Point(245, 142);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(111, 130);
            this.panel20.TabIndex = 54;
            this.panel20.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 97);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 24);
            this.label20.TabIndex = 50;
            this.label20.Text = "Prioridad: ";
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox19.Image = global::Problema2.Properties.Resources.barco2;
            this.pictureBox19.Location = new System.Drawing.Point(3, 3);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(95, 91);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 53;
            this.pictureBox19.TabStop = false;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Transparent;
            this.panel21.Controls.Add(this.label21);
            this.panel21.Controls.Add(this.pictureBox20);
            this.panel21.Location = new System.Drawing.Point(245, 278);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(111, 130);
            this.panel21.TabIndex = 54;
            this.panel21.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(3, 97);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(96, 24);
            this.label21.TabIndex = 50;
            this.label21.Text = "Prioridad: ";
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox20.Image = global::Problema2.Properties.Resources.barco2;
            this.pictureBox20.Location = new System.Drawing.Point(3, 3);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(95, 91);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 53;
            this.pictureBox20.TabStop = false;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Transparent;
            this.panel22.Controls.Add(this.label22);
            this.panel22.Controls.Add(this.pictureBox21);
            this.panel22.Location = new System.Drawing.Point(370, 278);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(111, 130);
            this.panel22.TabIndex = 54;
            this.panel22.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(3, 97);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 24);
            this.label22.TabIndex = 50;
            this.label22.Text = "Prioridad: ";
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox21.Image = global::Problema2.Properties.Resources.barco2;
            this.pictureBox21.Location = new System.Drawing.Point(3, 3);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(95, 91);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 53;
            this.pictureBox21.TabStop = false;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Transparent;
            this.panel23.Controls.Add(this.label23);
            this.panel23.Controls.Add(this.pictureBox22);
            this.panel23.Location = new System.Drawing.Point(394, 9);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(111, 130);
            this.panel23.TabIndex = 54;
            this.panel23.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(3, 97);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 24);
            this.label23.TabIndex = 50;
            this.label23.Text = "Prioridad: ";
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox22.Image = global::Problema2.Properties.Resources.barco2;
            this.pictureBox22.Location = new System.Drawing.Point(3, 3);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(95, 91);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 53;
            this.pictureBox22.TabStop = false;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.Transparent;
            this.panel24.Controls.Add(this.label24);
            this.panel24.Controls.Add(this.pictureBox23);
            this.panel24.Location = new System.Drawing.Point(918, 306);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(111, 130);
            this.panel24.TabIndex = 54;
            this.panel24.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(3, 97);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(96, 24);
            this.label24.TabIndex = 50;
            this.label24.Text = "Prioridad: ";
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox23.Image = global::Problema2.Properties.Resources.barco2;
            this.pictureBox23.Location = new System.Drawing.Point(3, 3);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(95, 91);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 53;
            this.pictureBox23.TabStop = false;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.Transparent;
            this.panel25.Controls.Add(this.label25);
            this.panel25.Controls.Add(this.pictureBox24);
            this.panel25.Location = new System.Drawing.Point(764, 403);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(134, 70);
            this.panel25.TabIndex = 46;
            this.panel25.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(3, 40);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 24);
            this.label25.TabIndex = 18;
            this.label25.Text = "Prioridad: ";
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox24.Image = global::Problema2.Properties.Resources.barco3;
            this.pictureBox24.Location = new System.Drawing.Point(3, 4);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(121, 33);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox24.TabIndex = 16;
            this.pictureBox24.TabStop = false;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Transparent;
            this.panel26.Controls.Add(this.label26);
            this.panel26.Controls.Add(this.pictureBox25);
            this.panel26.Location = new System.Drawing.Point(635, 395);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(123, 121);
            this.panel26.TabIndex = 48;
            this.panel26.Visible = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(3, 93);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(96, 24);
            this.label26.TabIndex = 19;
            this.label26.Text = "Prioridad: ";
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox25.Image = global::Problema2.Properties.Resources.barco1;
            this.pictureBox25.Location = new System.Drawing.Point(3, 3);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(95, 87);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox25.TabIndex = 22;
            this.pictureBox25.TabStop = false;
            // 
            // fInicial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BackgroundImage = global::Problema2.Properties.Resources.fondo;
            this.ClientSize = new System.Drawing.Size(1249, 487);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.CantidadC);
            this.Controls.Add(this.CantidadB);
            this.Controls.Add(this.cantidadD);
            this.Controls.Add(this.cantidadA);
            this.Controls.Add(this.lContador2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lContador);
            this.Controls.Add(this.flecha4);
            this.Controls.Add(this.flecha3);
            this.Controls.Add(this.flecha2);
            this.Controls.Add(this.flecha1);
            this.Controls.Add(this.verPosiciones);
            this.Controls.Add(this.pBarco3);
            this.Controls.Add(this.pBarco2);
            this.Controls.Add(this.pBarco1);
            this.Controls.Add(this.leyenda);
            this.Controls.Add(this.creaBarco);
            this.Controls.Add(this.pBarco4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "fInicial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Problema 2";
            ((System.ComponentModel.ISupportInitialize)(this.flecha1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flecha2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flecha3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flecha4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creaBarco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leyenda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.verPosiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco2A)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBarco3)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox flecha1;
        private System.Windows.Forms.PictureBox flecha2;
        private System.Windows.Forms.PictureBox flecha3;
        private System.Windows.Forms.PictureBox flecha4;
        private System.Windows.Forms.PictureBox creaBarco;
        private System.Windows.Forms.PictureBox leyenda;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pBarco1;
        private System.Windows.Forms.PictureBox pBarco2;
        private System.Windows.Forms.PictureBox pBarco4;
        private System.Windows.Forms.PictureBox verPosiciones;
        private System.Windows.Forms.PictureBox pBarco2A;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lContador;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lContador2;
        private System.Windows.Forms.Label cantidadA;
        private System.Windows.Forms.Label cantidadD;
        private System.Windows.Forms.Label CantidadB;
        private System.Windows.Forms.Label CantidadC;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pBarco3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox pictureBox25;
    }
}

