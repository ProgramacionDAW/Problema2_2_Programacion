﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      16/02/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Problema2
{
    public partial class fInicial : Form
    {
        #region Posiciones
        Point pos1 = new Point(157, 23);
        Point pos2 = new Point(157, 131);
        Point pos3 = new Point(157, 241);
        Point pos4 = new Point(157, 349);
        Point pos5 = new Point(356,162);
        Point pos6 = new Point(509,68);
        Point pos7 = new Point(509,280);
        Point pos8 = new Point(775,68);
        Point pos9 = new Point(775,280);
        Point pos10 = new Point(996,168);
        #endregion

        #region Imagenes Barcos
        List<Panel> barcos = new List<Panel>();
        #endregion

        #region Fases
        Fase1 fase1;
        Fase2 fase2;
        Fase3 fase3;
        Fase4 fase4;
        #endregion

        int contador = 0;
        int contador2 = 0;
        bool habilitarContador = false;
        bool habilitarContador2 = false;
        List<Barco> parteFinal = new List<Barco>();

        /// <summary>
        /// Constructor Form1 (fInicial)
        /// </summary>
        public fInicial()
        {
            InitializeComponent();
            Login login = new Login();
            login.ShowDialog();
            ObtenerFases();
            timer1.Start();
            timer2.Start();
            timer3.Start();
            #region AgregaBarcos
            //Ligeras
            barcos.Add(panel1);
            barcos.Add(panel2);
            barcos.Add(panel3);
            barcos.Add(panel4);
            barcos.Add(panel5);
            barcos.Add(panel6);
            //Cruceros
            barcos.Add(panel7);
            barcos.Add(panel8);
            barcos.Add(panel9);
            barcos.Add(panel10);
            barcos.Add(panel11);
            barcos.Add(panel12);
            //Mercantes
            barcos.Add(panel13);
            barcos.Add(panel14);
            barcos.Add(panel15);
            barcos.Add(panel16);
            barcos.Add(panel17);
            barcos.Add(panel18);
            //MercantesGiga
            barcos.Add(panel19);
            barcos.Add(panel20);
            barcos.Add(panel21);
            barcos.Add(panel22);
            barcos.Add(panel23);
            barcos.Add(panel24);
            //Otros
            barcos.Add(panel25);
            barcos.Add(panel26);
            #endregion
        }

        #region Cambio Colores
        //Métodos que cambian el color de los botones mostrando distintos PictureBoxs
        private void flecha1_MouseEnter(object sender, EventArgs e)
        {
            Bitmap flechaON = new Bitmap(Properties.Resources.f1ON);
            flecha1.Image = flechaON;
        }

        private void flecha1_MouseLeave(object sender, EventArgs e)
        {
            Bitmap flechaOFF = new Bitmap(Properties.Resources.f1OFF);
            flecha1.Image = flechaOFF;
        }

        private void flecha2_MouseEnter(object sender, EventArgs e)
        {
            Bitmap flechaON = new Bitmap(Properties.Resources.f2ON);
            flecha2.Image = flechaON;
        }

        private void flecha2_MouseLeave(object sender, EventArgs e)
        {
            Bitmap flechaOFF = new Bitmap(Properties.Resources.f2OFF);
            flecha2.Image = flechaOFF;
        }

        private void flecha3_MouseEnter(object sender, EventArgs e)
        {
            Bitmap flechaON = new Bitmap(Properties.Resources.f2ON);
            flecha3.Image = flechaON;
        }

        private void flecha3_MouseLeave(object sender, EventArgs e)
        {
            Bitmap flechaOFF = new Bitmap(Properties.Resources.f2OFF);
            flecha3.Image = flechaOFF;
        }

        private void flecha4_MouseEnter(object sender, EventArgs e)
        {
            Bitmap flechaON = new Bitmap(Properties.Resources.f4ON);
            flecha4.Image = flechaON;
        }

        private void flecha4_MouseLeave(object sender, EventArgs e)
        {
            Bitmap flechaOFF = new Bitmap(Properties.Resources.f4OFF);
            flecha4.Image = flechaOFF;
        }
        private void creaBarco_MouseEnter(object sender, EventArgs e)
        {
            Bitmap botonON = new Bitmap(Properties.Resources.creabarcoON);
            creaBarco.Image = botonON;
        }

        private void creaBarco_MouseLeave(object sender, EventArgs e)
        {
            Bitmap botonOFF = new Bitmap(Properties.Resources.creabarcoOFF);
            creaBarco.Image = botonOFF;
        }

        private void leyenda_MouseEnter(object sender, EventArgs e)
        {
            Bitmap botonON = new Bitmap(Properties.Resources.leyendaON);
            leyenda.Image = botonON;
        }

        private void leyenda_MouseLeave(object sender, EventArgs e)
        {
            Bitmap botonOFF = new Bitmap(Properties.Resources.leyendaOFF);
            leyenda.Image = botonOFF;
        }
        private void verPosiciones_MouseEnter(object sender, EventArgs e)
        {
            Bitmap botonON = new Bitmap(Properties.Resources.posicionesON);
            verPosiciones.Image = botonON;
        }

        private void verPosiciones_MouseLeave(object sender, EventArgs e)
        {
            Bitmap botonOFF = new Bitmap(Properties.Resources.posicionesOFF);
            verPosiciones.Image = botonOFF;
        }
        #endregion

        /// <summary>
        /// Método que muestra el formulario de Leyenda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void leyenda_Click(object sender, EventArgs e)
        {
            Leyenda formulario = new Leyenda();
            formulario.Show();
        }

        /// <summary>
        /// Método que muestra el formulario de CreaBarco
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void creaBarco_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            CreaBarco formulario = new CreaBarco(this);
            formulario.Show();
        }

        /// <summary>
        /// Método que muestra el formulario de Posiciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verPosiciones_Click(object sender, EventArgs e)
        {
            fPosiciones formulario = new fPosiciones();
            formulario.Show();
        }

        #region Metodos
        /// <summary>
        /// Método que da valor a las variables de fases del formulario
        /// </summary>
        private void ObtenerFases()
        {
            fase1 = Program.fase1;
            fase2 = Program.fase2;
            fase3 = Program.fase3;
            fase4 = Program.fase4;
        }

        /// <summary>
        /// Método que mueve los barcos ligeros a la fase 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoverBarcosA(object sender, EventArgs e)
        {
            Barco barco = fase1.SiguienteFase('A');
            if (barco!=null)
            {
                Panel imagen = ObtenerImagen(barco.Imagen);
                imagen.Location = pos1;
                imagen.Visible = true;
                flecha1.Visible = false;
                flecha2.Visible = false;
                flecha3.Visible = false;
                flecha4.Visible = false;
            }
        }

        /// <summary>
        /// Método que mueve los cruceros a la fase 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoverBarcosD(object sender, EventArgs e)
        {
            Barco barco = fase1.SiguienteFase('D');
            if (barco != null)
            {
                Panel imagen = ObtenerImagen(barco.Imagen);
                imagen.Location = pos2;
                imagen.Visible = true;
                flecha1.Visible = false;
                flecha2.Visible = false;
                flecha3.Visible = false;
                flecha4.Visible = false;
            }
        }

        /// <summary>
        /// Método que mueve los buques mercantes a la fase 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoverBarcosB(object sender, EventArgs e)
        {
            Barco barco = fase1.SiguienteFase('B');
            if (barco != null)
            {
                Panel imagen = ObtenerImagen(barco.Imagen);
                imagen.Location = pos3;
                imagen.Visible = true;
                flecha1.Visible = false;
                flecha2.Visible = false;
                flecha3.Visible = false;
                flecha4.Visible = false;
            }
        }

        /// <summary>
        /// Método que mueve los buques mercantes gigantes a la fase 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoverBarcosC(object sender, EventArgs e)
        {
            Barco barco = fase1.SiguienteFase('C');
            if (barco != null)
            {
                Panel imagen = ObtenerImagen(barco.Imagen);
                imagen.Location = pos4;
                imagen.Visible = true;
                flecha1.Visible = false;
                flecha2.Visible = false;
                flecha3.Visible = false;
                flecha4.Visible = false;
            }
        }

        /// <summary>
        /// Método que se encarga de mover todos los barcos de la fase 2
        /// </summary>
        private void MoverFase2()
        {
            if (fase2.Total>0)
            {
                Barco barco = fase2.Barcos[0];
                Panel imagen = ObtenerImagen(barco.Imagen);
                Point punto = new Point(imagen.Location.X, imagen.Location.Y);
                if (imagen.Location.X > pos5.X)
                {
                    imagen.Location = pos5;
                    barco.Posicion=5;
                }
                else
                {
                    switch (barco.Posicion)
                    {
                        case 1:
                            punto.X += 4;
                            punto.Y += 3;
                            imagen.Location = punto;
                            break;
                        case 2:
                            punto.X += 5;
                            punto.Y += 1;
                            imagen.Location = punto;
                            break;
                        case 3:
                            punto.X += 5;
                            punto.Y -= 2;
                            imagen.Location = punto;
                            break;
                        case 4:
                            punto.X += 4;
                            punto.Y -= 4;
                            imagen.Location = punto;
                            break;
                        case 5:
                            if (barco.Eslora<=250)
                            {
                                if (contador==0)
                                {
                                    fase2.SiguienteFase();
                                }
                            }
                            else
                            {
                                if (contador2==0)
                                {
                                    fase2.SiguienteFase();
                                }
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Método que se encarga de mover todos los barcos de la primera parte de la fase 3
        /// </summary>
        private void MoverFase3()
        {
            if (fase3.Total > 0)
            {
                foreach (Barco barco in fase3.Barcos)
                {
                    Panel imagen = ObtenerImagen(barco.Imagen);
                    Point punto = new Point(imagen.Location.X, imagen.Location.Y);
                    if (barco.Eslora<=250)
                    {
                        if (barco.Posicion==5)
                        {
                            if (imagen.Location.X >= pos6.X)
                            {
                                imagen.Location = pos6;
                                barco.Posicion = 6;
                                contador = 11;
                                habilitarContador = true;
                                flecha1.Visible = true;
                                flecha2.Visible = true;
                                flecha3.Visible = true;
                                flecha4.Visible = true;
                            }
                            else
                            {
                                punto.X += 3;
                                punto.Y -= 2;
                                imagen.Location = punto;
                            }
                        }
                    }
                    else
                    {
                        if (barco.Posicion==5)
                        {
                            if (imagen.Location.X >= pos7.X)
                            {
                                imagen.Location = pos7;
                                barco.Posicion = 7;
                                contador2 = 11;
                                habilitarContador2 = true;
                                flecha1.Visible = true;
                                flecha2.Visible = true;
                                flecha3.Visible = true;
                                flecha4.Visible = true;

                            }
                            else
                            {
                                punto.X += 4;
                                punto.Y += 3;
                                imagen.Location = punto;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Método que se encarga de mover todos los barcos de la segunda parte de la fase 3
        /// </summary>
        private void MoverFase3B()
        {
            if (fase3.Total > 0)
            {
                for (int i=0;i<fase3.Total;i++)
                {
                    Barco barco = fase3.Barcos[i];
                    Panel imagen = ObtenerImagen(barco.Imagen);
                    Point punto = new Point(imagen.Location.X, imagen.Location.Y);
                    switch (barco.Posicion)
                    {
                        case 6:
                            if (imagen.Location.X>pos8.X)
                            {
                                imagen.Location = pos8;
                                barco.Posicion = 8;
                            }
                            else
                            {
                                punto.X += 9;
                                punto.Y += 0;
                                imagen.Location = punto;
                            }
                            break;
                        case 7:
                            if (imagen.Location.X>pos9.X)
                            {
                                imagen.Location = pos9;
                                barco.Posicion = 9;
                            }
                            else
                            {
                                punto.X += 4;
                                punto.Y += 0;
                                imagen.Location = punto;
                            }
                            break;
                        case 8:
                            fase3.SiguienteFase(barco);
                            i--;
                            break;
                        case 9:
                            fase3.SiguienteFase(barco);
                            i--;
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Método que se encarga de mover todos los barcos de la fase 4
        /// </summary>
        private void MoverFase4()
        {
            if (fase4.Total>0)
            {
                if (fase4.Total==1)
                {
                    for (int i=0;i<fase4.Total;i++)
                    {
                        Barco barco = fase4.Barcos[i];
                        Panel imagen = ObtenerImagen(barco.Imagen);
                        Point punto = new Point(imagen.Location.X, imagen.Location.Y);
                        switch (barco.Posicion)
                        {
                            case 8:
                                if (imagen.Location.X > pos10.X)
                                {
                                    imagen.Location = pos10;
                                    barco.Posicion = 10;
                                }
                                else
                                {
                                    punto.X += 4;
                                    punto.Y += 2;
                                    imagen.Location = punto;
                                }
                                break;
                            case 9:
                                if (imagen.Location.X > pos10.X)
                                {
                                    imagen.Location = pos10;
                                    barco.Posicion = 10;
                                }
                                else
                                {
                                    punto.X += 4;
                                    punto.Y -= 2;
                                    imagen.Location = punto;
                                }
                                break;
                            case 10:
                                if (barco.Tipo == "Ligeras" || barco.Tipo == "Mercantes")
                                {
                                    punto.X += 2;
                                    punto.Y -= 2;
                                    imagen.Location = punto;
                                    parteFinal.Add(barco);
                                    fase4.EliminarBarco(barco);
                                    i--;
                                }
                                else
                                {
                                    punto.X += 2;
                                    punto.Y += 1;
                                    imagen.Location = punto;
                                    parteFinal.Add(barco);
                                    fase4.EliminarBarco(barco);
                                    i--;
                                }
                                break;
                        }
                    }
                }
                else
                {
                    Barco barco = fase4.ObtenerBarco();
                    Panel imagen = ObtenerImagen(barco.Imagen);
                    Point punto = new Point(imagen.Location.X, imagen.Location.Y);
                    switch (barco.Posicion)
                    {
                        case 8:
                            if (imagen.Location.X > pos10.X)
                            {
                                imagen.Location = pos10;
                                barco.Posicion = 10;
                            }
                            else
                            {
                                punto.X += 4;
                                punto.Y += 2;
                                imagen.Location = punto;
                            }
                            break;
                        case 9:
                            if (imagen.Location.X > pos10.X)
                            {
                                imagen.Location = pos10;
                                barco.Posicion = 10;
                            }
                            else
                            {
                                punto.X += 4;
                                punto.Y -= 2;
                                imagen.Location = punto;
                            }
                            break;
                        case 10:
                            if (barco.Tipo == "Ligeras" || barco.Tipo == "Mercantes")
                            {
                                punto.X += 2;
                                punto.Y -= 2;
                                imagen.Location = punto;
                                parteFinal.Add(barco);
                                fase4.EliminarBarco(barco);
                            }
                            else
                            {
                                punto.X += 2;
                                punto.Y += 1;
                                imagen.Location = punto;
                                parteFinal.Add(barco);
                                fase4.EliminarBarco(barco);
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Método que controla el contador de 10 segundos
        /// </summary>
        private void Contador()
        {
            if (habilitarContador==true)
            {
                if (contador > 0)
                {
                    contador--;
                }
                else
                {
                    habilitarContador = false;
                }
            }
            if (habilitarContador2 == true)
            {
                if (contador2 > 0)
                {
                    contador2--;
                }
                else
                {
                    habilitarContador2 = false;
                }
            }
            lContador.Text = string.Concat("Contador: ", contador.ToString());
            lContador2.Text = string.Concat("Contador: ", contador2.ToString());
        }

        /// <summary>
        /// Método que muestra la cantidad de barcos que hay en cada cola (Actualiza los labels)
        /// </summary>
        private void NumBarcos()
        {
            cantidadA.Text = Program.fase1.ColaA.ToString();
            CantidadB.Text = Program.fase1.ColaB.ToString();
            CantidadC.Text = Program.fase1.ColaC.ToString();
            cantidadD.Text = Program.fase1.ColaD.ToString();
        }

        /// <summary>
        /// Método que devuelve el panel correspondiente al nombre que se le pasa
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns>El control panel en caso de que exista</returns>
        private Panel ObtenerImagen(string nombre)
        {
            foreach (Panel imagen in barcos)
            {
                if (imagen.Name==nombre)
                {
                    return imagen;
                }
            }
            return null;
        }

        /// <summary>
        /// Método que mueve los barcos a un punto fuera de la ventana y los oculta finalizando el recorrido del barco
        /// </summary>
        private void FinalizarRecorrido()
        {
            for (int i=0;i<parteFinal.Count;i++)
            {
                Barco barco = parteFinal[i];
                Panel imagen = ObtenerImagen(barco.Imagen);
                Point punto = new Point(imagen.Location.X, imagen.Location.Y);
                Point posfinal = new Point(1280, imagen.Location.Y);
                if (barco.Tipo == "Ligeras" || barco.Tipo == "Mercantes")
                {
                    if (imagen.Location.X > posfinal.X)
                    {
                        imagen.Visible = false;
                        parteFinal.Remove(barco);
                    }
                    else
                    {
                        punto.X += 2;
                        punto.Y -= 2;
                        imagen.Location = punto;
                    }
                }
                else
                {
                    if (imagen.Location.X > posfinal.X)
                    {
                        imagen.Visible = false;
                        parteFinal.Remove(barco);
                    }
                    else
                    {
                        punto.X += 2;
                        punto.Y += 1;
                        imagen.Location = punto;
                    }
                }
            }
        }

        /// <summary>
        /// Método que fuerza al programa a que dos barcos lleguen a la vez. Contiene las variables de prioridad de cada barco.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MismoTiempo(object sender, EventArgs e)
        {
            button1.Visible = false;
            int prioridad1 = 1; //Prioridad del barco de la via rápida
            int prioridad2 = 1; //Prioridad del barco de la via lenta
            Barco barco = new Barco("Ligeras", 6, prioridad1, 134, "panel25");
            Barco barco2 = new Barco("Crucero", 7, prioridad2, 134, "panel26");
            Point arriba = new Point(774, 68);
            Point abajo = new Point(774, 280);
            foreach (Control control in panel25.Controls)
            {
                if (control is Label)
                {
                    control.Text = string.Concat("Prioridad: ", prioridad1.ToString());
                }
            }
            foreach (Control control in panel26.Controls)
            {
                if (control is Label)
                {
                    control.Text = string.Concat("Prioridad: ", prioridad2.ToString());
                }
            }
            panel25.Location = arriba;
            panel26.Location = abajo;
            panel25.Visible = true;
            panel26.Visible = true;
            fase3.AgregarBarco(barco);
            fase3.AgregarBarco(barco2);
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve la lista de paneles de los barcos
        /// </summary>
        public List<Panel> Barcos
        {
            get
            {
                return barcos;
            }
        }
        #endregion

        /// <summary>
        /// Método que controla el timer1. Tiene un intervalo de 100ms
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            MoverFase2();
            MoverFase3();
        }

        /// <summary>
        /// Método que controla el timer2. Tiene un intervalo de 1000ms
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            NumBarcos();
            Contador();
            MoverFase3B();
        }

        /// <summary>
        /// Método que controla el timer3. Tiene un intervalo de 1ms
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer3_Tick(object sender, EventArgs e)
        {
            MoverFase4();
            FinalizarRecorrido();
        }
    }
}
