﻿namespace Problema2
{
    partial class Leyenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barco3 = new System.Windows.Forms.PictureBox();
            this.titulo = new System.Windows.Forms.Label();
            this.barco1 = new System.Windows.Forms.PictureBox();
            this.barco2 = new System.Windows.Forms.PictureBox();
            this.barco4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.barco3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barco1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barco2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barco4)).BeginInit();
            this.SuspendLayout();
            // 
            // barco3
            // 
            this.barco3.BackColor = System.Drawing.Color.Transparent;
            this.barco3.Image = global::Problema2.Properties.Resources.barco1;
            this.barco3.Location = new System.Drawing.Point(21, 136);
            this.barco3.Name = "barco3";
            this.barco3.Size = new System.Drawing.Size(113, 99);
            this.barco3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.barco3.TabIndex = 0;
            this.barco3.TabStop = false;
            // 
            // titulo
            // 
            this.titulo.AutoSize = true;
            this.titulo.Font = new System.Drawing.Font("Source Sans Pro Semibold", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titulo.ForeColor = System.Drawing.Color.DarkRed;
            this.titulo.Location = new System.Drawing.Point(226, 21);
            this.titulo.Name = "titulo";
            this.titulo.Size = new System.Drawing.Size(142, 40);
            this.titulo.TabIndex = 1;
            this.titulo.Text = "LEYENDA";
            // 
            // barco1
            // 
            this.barco1.BackColor = System.Drawing.Color.Transparent;
            this.barco1.Image = global::Problema2.Properties.Resources.barco3;
            this.barco1.Location = new System.Drawing.Point(21, 90);
            this.barco1.Name = "barco1";
            this.barco1.Size = new System.Drawing.Size(113, 35);
            this.barco1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.barco1.TabIndex = 2;
            this.barco1.TabStop = false;
            // 
            // barco2
            // 
            this.barco2.BackColor = System.Drawing.Color.Transparent;
            this.barco2.Image = global::Problema2.Properties.Resources.barco4;
            this.barco2.Location = new System.Drawing.Point(21, 246);
            this.barco2.Name = "barco2";
            this.barco2.Size = new System.Drawing.Size(113, 86);
            this.barco2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.barco2.TabIndex = 3;
            this.barco2.TabStop = false;
            // 
            // barco4
            // 
            this.barco4.BackColor = System.Drawing.Color.Transparent;
            this.barco4.Image = global::Problema2.Properties.Resources.barco2;
            this.barco4.Location = new System.Drawing.Point(21, 338);
            this.barco4.Name = "barco4";
            this.barco4.Size = new System.Drawing.Size(113, 98);
            this.barco4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.barco4.TabIndex = 4;
            this.barco4.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(149, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 46);
            this.label2.TabIndex = 6;
            this.label2.Text = "→";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(149, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 46);
            this.label3.TabIndex = 7;
            this.label3.Text = "→";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(149, 362);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 46);
            this.label4.TabIndex = 8;
            this.label4.Text = "→";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(149, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 46);
            this.label5.TabIndex = 9;
            this.label5.Text = "→";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Source Sans Pro", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DarkRed;
            this.label7.Location = new System.Drawing.Point(204, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(323, 40);
            this.label7.TabIndex = 11;
            this.label7.Text = "Embarcaciones ligeras ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Source Sans Pro", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkRed;
            this.label6.Location = new System.Drawing.Point(204, 261);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(272, 40);
            this.label6.TabIndex = 12;
            this.label6.Text = "Buques mercantes ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Source Sans Pro", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DarkRed;
            this.label8.Location = new System.Drawing.Point(204, 368);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(397, 40);
            this.label8.TabIndex = 13;
            this.label8.Text = "Buques mercantes gigantes  ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Source Sans Pro", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DarkRed;
            this.label9.Location = new System.Drawing.Point(204, 160);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 40);
            this.label9.TabIndex = 14;
            this.label9.Text = "Cruceros";
            // 
            // Leyenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(610, 458);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.barco4);
            this.Controls.Add(this.barco2);
            this.Controls.Add(this.barco1);
            this.Controls.Add(this.titulo);
            this.Controls.Add(this.barco3);
            this.Name = "Leyenda";
            this.Text = "Leyenda";
            ((System.ComponentModel.ISupportInitialize)(this.barco3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barco1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barco2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barco4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox barco3;
        private System.Windows.Forms.Label titulo;
        private System.Windows.Forms.PictureBox barco1;
        private System.Windows.Forms.PictureBox barco2;
        private System.Windows.Forms.PictureBox barco4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}