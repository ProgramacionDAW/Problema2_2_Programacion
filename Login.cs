﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      16/02/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Problema2
{
    public partial class Login : Form
    {
        /// <summary>
        /// Constructor del formulario Login
        /// </summary>
        public Login()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Método que muestra la contraseña
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("La contraseña es 1234", "Ayuda", MessageBoxButtons.OK);
        }

        /// <summary>
        /// Método que comprueba la validez de los datos introducidos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (password.Text=="1234")
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Contraseña incorrecta", "Error", MessageBoxButtons.OK);
                password.Clear();
                DialogResult = DialogResult.None;
            }
        }

        /// <summary>
        /// Método que controla el Enter en el Textbox. Comprueba la validez de los datos introducidos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void password_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (password.Text == "1234")
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Contraseña incorrecta", "Error", MessageBoxButtons.OK);
                    password.Clear();
                    DialogResult = DialogResult.None;
                }
            }
        }

        /// <summary>
        /// Método que cambia el cursor al pasarlo por el control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            pictureBox2.Cursor = Cursors.Hand;
        }

        /// <summary>
        /// Método que cambia el cursor al salir del control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            pictureBox2.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Método que controla el boton Salir. Finaliza el programa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
